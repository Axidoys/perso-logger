package com.persologger.persologger

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PersoLoggerApplication

fun main(args: Array<String>) {
	runApplication<PersoLoggerApplication>(*args)
}

