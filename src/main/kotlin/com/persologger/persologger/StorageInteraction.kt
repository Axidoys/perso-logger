package com.persologger.persologger.storage

import com.persologger.persologger.dto.Project
import com.persologger.persologger.dto.TimeLog
import org.ktorm.database.Database
import org.ktorm.dsl.*
import org.ktorm.entity.sequenceOf
import org.ktorm.entity.toList
import org.ktorm.schema.*
import org.springframework.stereotype.Service
import java.time.LocalDateTime

enum class TimerState { ACTIVE, PAUSED, STOPPED }

object Projects : Table<Project>("t_project") {
    val id = int("id").primaryKey().bindTo { it.id }
    val name = varchar("name").bindTo { it.name }
    val timerState = enum<TimerState>("timer_state").bindTo { it.timerState }
    val lastTimer = datetime("last_timer").bindTo { it.lastTimer }
    val totalTime = int("total_time").bindTo { it.totalTime }
}

object TimeLogs : Table<TimeLog>("t_time_log") {
    val id = int("id").primaryKey()
    val projectId = int("project_id").references(Projects) { it.project }
    val dateTime = datetime("date_time").bindTo { it.dateTime }
    val duration = int("duration").bindTo { it.duration }
}

@Service
class StorageInteraction {

    final val database = Database.connect(
        url = "jdbc:sqlite:db.sqlite",
        user = "root",
        password = "***"
    )

    init {
        database.useConnection { connection ->
            connection.createStatement().execute(
                """                
                CREATE TABLE IF NOT EXISTS t_project (
                     id integer PRIMARY KEY AUTOINCREMENT,
                     name varchar NOT NULL,
                     timer_state varchar NOT NULL,
                     last_timer varchar,
                     total_time integer
                );
            """
            )
            connection.createStatement().execute(
                """                
                     CREATE TABLE IF NOT EXISTS t_time_log (
                     id integer PRIMARY KEY AUTOINCREMENT,
                     date_time varchar NOT NULL,
                     duration int NOT NULL,
                     project_id integer NOT NULL,
                     FOREIGN KEY(project_id) REFERENCES t_project(id)
                );
            """
            )


        }
    }

    fun createNewProject(name: String): Boolean {
        if (projectAlreadyExists(name)){
            return false
        }

        database.insertAndGenerateKey(Projects){
            set(it.name, name)
            set(it.timerState, TimerState.STOPPED)
            set(it.totalTime, 0)
        }

        return true
    }

    private fun projectAlreadyExists(name: String) = (database.from(Projects)
        .select()
        .where { Projects.name eq name }
        .limit(1)
        .totalRecords
            > 0)

    fun retrieveProject(name: String): Project {
        return Projects.createEntity(
            database.from(Projects)
                .select()
                .where { Projects.name eq name }
                .iterator().next()
        )
    }

    fun retrieveProjects(): List<Project> {
        return database.sequenceOf(Projects).toList()
    }

    fun removeProject(name: String): Boolean {
        return database.delete(Projects) { project -> project.name.eq(name) } > 0
    }

    fun changeTimerState(name: String, newState: TimerState, lastDate: LocalDateTime?, addedTime: Int?) {
        database.update(Projects) {
            set(it.timerState, newState)
            set(it.lastTimer, lastDate)
            if (addedTime != null) set(it.totalTime, it.totalTime + addedTime)
            where {
                it.name eq name
            }
        }
    }

    fun retrieveTimeLogs(): List<TimeLog> {
        return database.sequenceOf(TimeLogs).toList()
    }

    fun addNewAnalyticWorklog(projectId: Int, dateTime: LocalDateTime, duration: Int){
        database.insertAndGenerateKey(TimeLogs){
            set(it.projectId, projectId)
            set(it.dateTime, dateTime)
            set(it.duration, duration)
        }
    }
}