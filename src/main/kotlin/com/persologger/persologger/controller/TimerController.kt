package com.persologger.persologger.controller

import com.persologger.persologger.TimerInteraction
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletResponse

@CrossOrigin
@RestController
class TimerController(val timerInteraction: TimerInteraction) {
    companion object {
        @Suppress("JAVA_CLASS_ON_COMPANION")
        @JvmStatic
        private val logger = LoggerFactory.getLogger(javaClass.enclosingClass)
    }

    @PutMapping("/start", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun new(
        @RequestBody projectName: ProjectName,
        response: HttpServletResponse
    ) : String {
        logger.info("request {start} for project named : ${projectName.name}")

        if(!timerInteraction.startTimer(projectName.name)){
            response.status = 403
            //TODO 404 project not found, not handled now
            return "Timer issue"
        }

        response.status = 200
        return "Timer started"
    }

    @PutMapping("/stop", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun stop(
        @RequestBody projectName: ProjectName,
        response: HttpServletResponse
    ) : String {
        logger.info("request {stop} for project named : ${projectName.name}")

        if(!timerInteraction.stopTimer(projectName.name)){
            response.status = 403
            //TODO 404 project not found, not handled now
            return "Timer issue"
        }

        response.status = 200
        return "Timer stopped"
    }
}