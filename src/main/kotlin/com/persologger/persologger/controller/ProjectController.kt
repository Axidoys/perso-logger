package com.persologger.persologger.controller

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.persologger.persologger.dto.ProjectApi
import com.persologger.persologger.storage.StorageInteraction
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.web.bind.annotation.*
import java.time.format.DateTimeFormatter
import javax.servlet.http.HttpServletResponse

val mapper = jacksonObjectMapper()

data class ProjectName(val name: String)

@CrossOrigin
@RestController
class ProjectController(val storageInteraction: StorageInteraction) {

    companion object {
        @Suppress("JAVA_CLASS_ON_COMPANION")
        @JvmStatic
        private val logger = LoggerFactory.getLogger(javaClass.enclosingClass)
    }

    @PostMapping("/new", consumes = [APPLICATION_JSON_VALUE])
    fun new(
        @RequestBody projectName: ProjectName,
        response: HttpServletResponse
    ) : String {
        logger.info("request {new} for project named : ${projectName.name}")

        if(!storageInteraction.createNewProject(projectName.name)){
            response.status = 403
            return "Project already exist"
        }

        response.status = 201
        return "Project created"
    }

    @DeleteMapping("/remove", consumes = [APPLICATION_JSON_VALUE])
    fun remove(
        @RequestBody projectName: ProjectName,
        response: HttpServletResponse
    ): String {
        logger.info("request {remove} for project named : ${projectName.name}")

        if(!storageInteraction.removeProject(projectName.name)){
            response.status = 404
            return "Project does not exist"
        }

        response.status = 200
        return "Project removed"
    }

    @GetMapping("/list", produces = ["application/json"])
    fun list(
    ): String {
        logger.info("request {list}")

        val projectList = storageInteraction.retrieveProjects()
            .stream()
            .map { project ->
                ProjectApi(
                    project.name,
                    project.timerState.toString(),
                    if (project.lastTimer != null) project.lastTimer!!.format(DateTimeFormatter.ISO_DATE_TIME) else "-",
                    project.totalTime
                )
            }
            .toList()
        return mapper.writeValueAsString(projectList)
    }

}