package com.persologger.persologger.controller

import com.persologger.persologger.dto.AnalyticsApi
import com.persologger.persologger.dto.WeekDetails
import com.persologger.persologger.storage.StorageInteraction
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.time.DayOfWeek
import java.time.LocalDateTime
import java.time.temporal.ChronoField
import kotlin.math.ceil

@CrossOrigin
@RestController
class AnalyticsController(val storageInteraction: StorageInteraction) {
    companion object {
        @Suppress("JAVA_CLASS_ON_COMPANION")
        @JvmStatic
        private val logger = LoggerFactory.getLogger(javaClass.enclosingClass)
    }

    @GetMapping("/analytics", produces = ["application/json"])
    fun getAnalytics(
    ) : String {
        logger.info("request {analytics}")

        val now = LocalDateTime.now()
        val startOfWeek = now
            .minusDays(now.dayOfWeek.getLong(ChronoField.DAY_OF_WEEK)-1L)
            .withHour(0)
            .withMinute(0)
            .withSecond(0)
            .withNano(0)
        val startOfLastWeek = startOfWeek.minusWeeks(1)

        val analyticsList = storageInteraction.retrieveTimeLogs()
            .groupBy { t -> t.project.name }
            .map { tlogs ->
                val thisWeek = tlogs.value.filter { it.dateTime isAfter startOfWeek }
                val lastWeek = tlogs.value.filter { it.dateTime isAfter startOfLastWeek && it.dateTime isBefore startOfWeek }
                AnalyticsApi(
                    projectName = tlogs.key,
                    totalCurrentWeek = thisWeek.sumOf { it.duration }.secToMin(),
                    totalLastWeek = lastWeek.sumOf { it.duration }.secToMin(),
                    currentWeekDetails = WeekDetails(
                        monday = thisWeek.filter { it.dateTime on DayOfWeek.MONDAY }.sumOf { it.duration }.secToMin(),
                        tuesday = thisWeek.filter { it.dateTime on DayOfWeek.TUESDAY }.sumOf { it.duration }.secToMin(),
                        wednesday = thisWeek.filter { it.dateTime on DayOfWeek.WEDNESDAY }.sumOf { it.duration }.secToMin(),
                        thursday = thisWeek.filter { it.dateTime on DayOfWeek.THURSDAY }.sumOf { it.duration }.secToMin(),
                        friday = thisWeek.filter { it.dateTime on DayOfWeek.FRIDAY }.sumOf { it.duration }.secToMin(),
                        saturday = thisWeek.filter { it.dateTime on DayOfWeek.SATURDAY }.sumOf { it.duration }.secToMin(),
                        sunday = thisWeek.filter { it.dateTime on DayOfWeek.SUNDAY }.sumOf { it.duration }.secToMin()
                    )
                )
            }
            .toList()

        return mapper.writeValueAsString(analyticsList)
    }
}

private fun Int.secToMin(): Int {
    return ceil(this.toDouble()/60.0).toInt()
}

infix fun LocalDateTime.isBefore(dateTime: LocalDateTime) = this < dateTime

infix fun LocalDateTime.isAfter(dateTime: LocalDateTime) = this > dateTime

infix fun LocalDateTime.on(day: DayOfWeek) = this.dayOfWeek == day


