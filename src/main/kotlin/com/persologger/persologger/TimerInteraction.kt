package com.persologger.persologger

import com.persologger.persologger.storage.StorageInteraction
import com.persologger.persologger.storage.TimerState
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import java.time.ZoneOffset

@Component
class TimerInteraction(val storageInteraction: StorageInteraction) {
    fun startTimer(name: String): Boolean {
        val project = storageInteraction.retrieveProject(name)

        if(project.timerState==TimerState.ACTIVE) {
            return false
        }

        storageInteraction.changeTimerState(name, TimerState.ACTIVE, LocalDateTime.now(), 0)
        return true
    }

    fun stopTimer(name: String): Boolean {
        val project = storageInteraction.retrieveProject(name)

        if(project.timerState!=TimerState.ACTIVE) {
            return false
        }

        val addedTime = LocalDateTime.now() - project.lastTimer!!
        storageInteraction.changeTimerState(name, TimerState.STOPPED, null, addedTime)

        storageInteraction.addNewAnalyticWorklog(
            project.id,
            project.lastTimer!!,
            addedTime
        )
        return true
    }
}

private operator fun LocalDateTime.minus(oldestTimer: LocalDateTime): Int {
    return (this.toEpochSecond(ZoneOffset.UTC) - oldestTimer.toEpochSecond(ZoneOffset.UTC)).toInt()
}
