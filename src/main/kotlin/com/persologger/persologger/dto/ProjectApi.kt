package com.persologger.persologger.dto

data class ProjectApi(
    val name: String,
    val timerState: String,
    val lastTimer: String,
    val totalTime: Int
)