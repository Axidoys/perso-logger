package com.persologger.persologger.dto

data class AnalyticsApi(
    val projectName: String,
    val totalCurrentWeek: Int,
    val totalLastWeek: Int,
    val currentWeekDetails: WeekDetails
)

data class WeekDetails(
    val monday: Int = 0,
    val tuesday: Int = 0,
    val wednesday: Int = 0,
    val thursday: Int = 0,
    val friday: Int = 0,
    val saturday: Int = 0,
    val sunday: Int = 0
)
