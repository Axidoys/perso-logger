package com.persologger.persologger.dto

import com.persologger.persologger.storage.TimerState
import org.ktorm.entity.Entity
import java.time.LocalDateTime

interface Project : Entity<Project> {
    val id: Int
    val name: String
    val timerState: TimerState
    val lastTimer: LocalDateTime?
    val totalTime: Int
}