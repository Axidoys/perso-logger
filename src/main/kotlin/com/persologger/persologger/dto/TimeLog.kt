package com.persologger.persologger.dto

import org.ktorm.entity.Entity
import java.time.LocalDateTime

interface TimeLog: Entity<TimeLog> {
    val project: Project
    val dateTime: LocalDateTime
    val duration: Int
}

