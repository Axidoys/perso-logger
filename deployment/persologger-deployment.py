#!/usr/bin/env python3
from http.server import BaseHTTPRequestHandler, HTTPServer
import logging
import os

DEFAULT_PORT = 8071

class S(BaseHTTPRequestHandler):
    def _set_response(self, http_code):
        self.send_response(http_code)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_POST(self):
        logging.info("POST request,\nPath: %s\nHeaders:\n%s\n",
                str(self.path), str(self.headers))

        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))

        # Verification du token
        if self.headers.get("X-Gitlab-Token") != os.getenv('PERSO_LOGGER_CD_TOKEN'):
            # token pas bon => on ne fait pas le déploiement
            self._set_response(401)
            return

        os.system('docker-compose pull')
        os.system('docker-compose up --force-recreate -d')

        os.system('curl -H "Content-Type: application/json" -d \'{"content": "Deploiement effectue"}\' https://discord.com/api/webhooks/' + os.getenv('PERSO_LOGGER_DISCORD_HOOK_URL'))

        self._set_response(200)

def run(server_class=HTTPServer, handler_class=S):
    logging.basicConfig(level=logging.INFO)
    server_address = ('', DEFAULT_PORT)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting perso logger continuous deployment daemon...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logging.info('Stopping daemon...\n')

if __name__ == '__main__':
    run()
